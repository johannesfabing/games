package bid.cheetahx7x.FirstGame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class FirstGame extends BasicGame {

	public FirstGame() {
		super("FirstGame");
	}
	
	double gamespeed = 0.1;
	double angle = 0;
	double rectx = 100;
	double recty = 200;
	double circx = 200;
	Boolean circleft = false;
	double ovaly = 100;
	Boolean ovalonscreen = true;

	public static void main(String[] argv) {
		try {
			AppGameContainer gc = new AppGameContainer(new FirstGame());
			gc.setDisplayMode(800, 600, false);
			gc.start();
			gc.setVSync(true);
			gc.setTargetFrameRate(120);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		g.drawRect((int) this.rectx, (int) this.recty, 50, 50);
		g.drawOval((int) this.circx, 400, 100, 100);
		g.drawOval(400, (int) this.ovaly, 200, 50);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		if (circx <= 0) {
			circleft = false;
		} else if (circx >= 700) {
			circleft = true;
		}

		if (ovaly <= 0) {
			this.ovalonscreen = false;
		}

		if (circleft == false) {
			circx += delta * gamespeed;
		} else {
			circx -= delta * gamespeed;
		}
		if (ovalonscreen == true) {
			ovaly -= delta * gamespeed;
		} else {
			ovaly = 600;
			this.ovalonscreen = true;
		}

		if (angle >= 360) {
			angle = 0;
		} else {
			angle += delta * gamespeed;
		}

		this.rectx = (Math.cos((angle * Math.PI) / 180) * 100) + 200;
		this.recty = (Math.sin((angle * Math.PI) / 180) * 100) + 200;
		
		
		
		
		
		
		
	}
}
