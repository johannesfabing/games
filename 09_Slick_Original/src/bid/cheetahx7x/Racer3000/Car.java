package bid.cheetahx7x.Racer3000;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Car implements Actors{
	
	private float currentspeed;
	private Image cartexture;
	private Cartype car;
	
	public Car(Cartype car) {
		super();
		this.car = car;
	}
	
	public void update(int delta, double speed, GameContainer gc, boolean left, boolean right, boolean up)
	{
		
	}
	
	public void render(Graphics g)
	{
		
	}

	public Cartype getCar() {
		return car;
	}

	public void setCar(Cartype car) {
		this.car = car;
	}
	
	public float getCurrentspeed() {
		return currentspeed;
	}

	public void setCurrentspeed(float speed) {
		this.currentspeed = speed;
	}
	

	
}
