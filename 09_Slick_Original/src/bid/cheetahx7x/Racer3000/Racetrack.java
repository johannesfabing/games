package bid.cheetahx7x.Racer3000;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Point;

public class Racetrack implements Actors {

	private Image map;
	private float angle;
	private Car car;
	private Point carlocation = new Point(500, 400);

	public Racetrack(Car car) {
		super();
		this.car = car;
	}

	public void init(GameContainer gc) throws SlickException {
		map = new Image("C:\\Users\\jonyf\\git\\games\\09_Slick_Original\\src\\bid\\cheetahx7x\\Racer3000\\map1.png");
		map.draw(carlocation.getX(), carlocation.getY());
	}

	public void update(int delta, double speed, GameContainer gc, boolean left, boolean right, boolean up) {
		setAngle(delta, left, right);
		map.setCenterOfRotation(gc.getWidth() / 2, gc.getHeight() / 2);
		map.rotate(angle);
		if(up) {
			this.getLocation(delta, speed);
			System.out.println(carlocation.getX()+" "+carlocation.getY());
			System.out.println(map.getRotation());
		}
	}

	public void render(Graphics g) {
		map.draw(carlocation.getX(), carlocation.getY());
	}

	private void setAngle(int delta, boolean left, boolean right) {
		if (left) {
			this.angle -= delta * car.getCar().getTurnspeed();
		} else if (right) {
			this.angle += delta * car.getCar().getTurnspeed();
		} else if (this.angle < 0) {
			this.angle += delta * car.getCar().getTurnspeed();
			if (this.angle > 0) {
				this.angle = 0;
			}
		} else if (this.angle > 0) {
			this.angle -= delta * car.getCar().getTurnspeed();
			if (this.angle < 0) {
				this.angle = 0;
			}
		} else {

		}
	}

	public float getAngle() {
		return angle;
	}

	private void getLocation(int delta, double speed) {
		carlocation.setX((float) (carlocation.getX() + Math.sin((((map.getRotation()*Math.PI)/180)/4)) * speed * delta));
		carlocation.setY((float) (carlocation.getY() + Math.cos((((map.getRotation()*Math.PI)/180)/4)) * speed * delta));
	}
}
