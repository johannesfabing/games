package bid.cheetahx7x.Racer3000;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Racer3000 extends BasicGame {

	public Racer3000() {
		super("Racer3000");
	}

	double gamespeed = 0.1;
	KeyInputs inputs = new KeyInputs();
	List<Actors> actors;

	public static void main(String[] argv) {
		try {
			AppGameContainer gc = new AppGameContainer(new Racer3000());
			gc.setDisplayMode(800, 600, false);
			gc.start();
			gc.setVSync(true);
			gc.setTargetFrameRate(120);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void init(GameContainer gc) throws SlickException {
		List<Cartype> cartypes = new ArrayList<Cartype>();
		cartypes.add(new Cartype());
		Car car = new Car(cartypes.get(0));
		Racetrack racetrack = new Racetrack(car);
		racetrack.init(gc);
		actors = new ArrayList<Actors>();
		actors.add(racetrack);
		actors.add(car);
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		for(Actors actor : actors) {
			actor.render(g);
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for(Actors actor : actors) {
			actor.update(delta, gamespeed, gc, inputs.Left(gc), inputs.Right(gc), inputs.Up(gc));
		}
	}
}
