package bid.cheetahx7x.Racer3000;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;

public class KeyInputs {
	@SuppressWarnings("unused")
	public boolean Up(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_UP);
	}
	
	@SuppressWarnings("unused")
	public boolean Left(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_LEFT);
	}
	
	@SuppressWarnings("unused")
	public boolean Right(GameContainer gc) {
		return gc.getInput().isKeyDown(Input.KEY_RIGHT);
	}
}
