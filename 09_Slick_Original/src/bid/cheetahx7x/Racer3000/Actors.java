package bid.cheetahx7x.Racer3000;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public interface Actors {
	
	public void update(int delta, double speed, GameContainer gc, boolean left, boolean right, boolean up) throws SlickException;
	
	public void render(Graphics g);
}
