package bid.cheetahx7x.Racer3000;

public class Cartype {

	private String name = "Test";
	private float speed = (float)1;
	private float maxspeed = 1;
	//maxspeed upgrades
	private float turnspeed = (float)0.00001;
	//max turnspeed
	
	private int skin;

	public String getName() {
		return name;
	}

	public float getSpeed() {
		return speed;
	}

	public float getMaxspeed() {
		return maxspeed;
	}

	public float getTurnspeed() {
		return turnspeed;
	}

	public int getSkin() {
		return skin;
	}
}
