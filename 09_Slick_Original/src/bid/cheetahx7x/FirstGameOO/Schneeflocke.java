package bid.cheetahx7x.FirstGameOO;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Schneeflocke {
	public enum size {
		small, medium, large;
	}
	private Random r = new Random();
	private double x,y;
	private size size;
	
	public Schneeflocke(size size, int width) {
		super();
		this.x = r.nextDouble() * width;
		this.y = r.nextDouble() * -1000;
		this.size = size;
	}

	public void render(Graphics g) {
		if(size == size.small) {
			g.drawOval((int) this.x, (int) this.y, 7, 7);
		}
		else if(size == size.medium) {
			g.drawOval((int) this.x, (int) this.y, 10, 10);
		}
		else
		{
			g.drawOval((int) this.x, (int) this.y, 20, 20);
		}
	}

	public void update(int delta, GameContainer gc) {
		if(size == size.small) {
			this.y += delta * 0.1;
		}
		else if(size == size.medium) {
			this.y += delta * 0.2;
		}
		else
		{
			this.y += delta * 0.4;
		}
		
		if(this.y > gc.getScreenHeight()+5) {
			this.y = r.nextDouble() * -100;
			this.x = r.nextDouble() * gc.getScreenWidth();
		}
	}
	
	
	
	
	
	
}
