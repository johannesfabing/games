package bid.cheetahx7x.FirstGameOO;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLCircle {
	private double x, y;
	private int radius;
	private Boolean left = false;

	public HTLCircle(double x, double y, int radius) {
		super();
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void render(Graphics g) {
		g.drawOval((int) this.x, (int) this.y, this.radius, this.radius);
	}

	public void update(int delta, double speed, GameContainer gc) {
		if (this.x <= 0) {
			this.left = false;
		} else if (this.x >= gc.getWidth() - radius) {
			this.left = true;
		}

		if (this.left == false) {
			this.x += delta * speed;
		} else {
			this.x -= delta * speed;
		}
	}
}
