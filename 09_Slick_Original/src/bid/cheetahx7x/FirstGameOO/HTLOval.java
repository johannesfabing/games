package bid.cheetahx7x.FirstGameOO;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLOval {
	private double x, y;
	private int width, height;
	private Boolean up;
	private Boolean onscreen = true;

	public HTLOval(double x, double y, int width, int height, Boolean up) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.up = up;
	}

	public void render(Graphics g) {
		g.drawOval(400, (int) this.y, width, height);
	}

	public void update(int delta, double speed, GameContainer gc) {
		if ((int)this.y < 0 || this.y >= gc.getHeight()) {
			this.onscreen = false;
		}

		if (this.onscreen == true) {
			if (this.up == true) {
				this.y -= delta * speed;
			} else {
				this.y += delta * speed;
			}
		} else {
			if (this.up == true) {
				this.y = gc.getHeight() - height;
			} else {
				this.y = 0;
			}

			this.onscreen = true;
		}

	}
}
