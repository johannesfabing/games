package bid.cheetahx7x.FirstGameOO;

import org.newdawn.slick.Graphics;

public class HTLRectangle {
	private double angle = 0;
	private int width, height;
	private double x, y;
	private double xnow, ynow;
	private int radius;

	public HTLRectangle(int width, int height, double x, double y, int radius) {
		super();
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public void render(Graphics g) {
		g.drawRect((int) this.xnow, (int) this.ynow, this.width, this.height);
	}

	public void update(int delta, double speed) {

		if (angle >= 360) {
			angle = 0;
		} else {
			angle += delta * speed;
		}

		this.xnow = (Math.cos((angle * Math.PI) / 180) * radius) + this.x;
		this.ynow = (Math.sin((angle * Math.PI) / 180) * radius) + this.y;
	}

}
