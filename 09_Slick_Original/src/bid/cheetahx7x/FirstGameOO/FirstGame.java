package bid.cheetahx7x.FirstGameOO;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class FirstGame extends BasicGame {

	public FirstGame() {
		super("FirstGame");
	}

	HTLRectangle rectangle;
	HTLRectangle rectangle2;
	HTLCircle circle;
	HTLOval oval;
	List<Schneeflocke> flakes;

	double gamespeed = 0.1;

	public static void main(String[] argv) {
		try {
			AppGameContainer gc = new AppGameContainer(new FirstGame());
			gc.setDisplayMode(800, 600, false);
			gc.start();
			gc.setVSync(true);
			gc.setTargetFrameRate(120);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		rectangle2.render(g);
		rectangle.render(g);
		circle.render(g);
		oval.render(g);
		for(Schneeflocke flocke : flakes) {
			flocke.render(g);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		rectangle2 = new HTLRectangle(100, 200, 200, 300, 100);
		rectangle = new HTLRectangle(200, 100, 300, 200, 200);
		circle = new HTLCircle(300, 400, 100);
		oval = new HTLOval(400, 200, 200, 100, true);
		flakes = new ArrayList<Schneeflocke>();
		for(int i = 0; i< 2000; i++)
		{
			flakes.add(new Schneeflocke(Schneeflocke.size.small, gc.getScreenWidth()));
		}
		for(int i = 0; i< 1500; i++)
		{
			flakes.add(new Schneeflocke(Schneeflocke.size.medium, gc.getScreenWidth()));
		}
		for(int i = 0; i< 1000; i++)
		{
			flakes.add(new Schneeflocke(Schneeflocke.size.large, gc.getScreenWidth()));
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		rectangle2.update(delta, gamespeed);
		rectangle.update(delta, gamespeed);
		circle.update(delta, gamespeed, gc);
		oval.update(delta, gamespeed, gc);
		for(Schneeflocke flocke : flakes) {
			flocke.update(delta, gc);
		}
	}
}
